# springboot-cacheable

#### 功能介绍
1)SpringBoot通过@Cacheable注解实现缓存功能

2)ES增删改查

3)自定义权限过滤器

4)多线程事务回滚

5)利用Semaphore实现多线程调用接口A且限制接口A的每秒QPS为10

6)利用hutool工具类实现验证码功能
