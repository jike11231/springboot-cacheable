package com.it.controller;

import com.it.entity.User;
import com.it.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 控制层
 *
 * @author hua
 */
@RestController
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private AdminService AdminService;

    @PostMapping("/insert1")
    public void insert1() {
        AdminService.insert1();
    }

    @PostMapping("insert2")
    public void insert2(@RequestBody User user) {
        AdminService.insert2(user);
    }

    @PostMapping("insert3")
    public void insert2() {
        AdminService.insert3();
    }

    @PostMapping("getList")
    public List<String> getList(String skip) {
        return AdminService.getList(skip);
    }
}
