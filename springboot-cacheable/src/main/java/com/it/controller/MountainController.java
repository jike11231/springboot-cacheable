package com.it.controller;

import com.it.entity.Mountain;
import com.it.service.UserMountainService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 上古神山控制层
 *
 * @author hua
 */
@RestController
@RequestMapping(value = "/mountain")
public class MountainController {
    @Autowired
    UserMountainService userMountainService;

    @GetMapping(value = "/getMountainUserInfo")
    public ResponseEntity<List<Mountain>> getUserInfo() {
        List<Mountain> users = userMountainService.queryAllUser();
        HttpStatus status = users == null ? HttpStatus.NOT_FOUND: HttpStatus.OK;
        return new ResponseEntity<>(users, status);
    }
}
