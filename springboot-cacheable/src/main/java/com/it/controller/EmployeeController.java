package com.it.controller;

import com.it.entity.Employee;
import com.it.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * EmployeeController
 */
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping("/saveThreadByTransactional")
    public ResponseEntity saveThreadByTransactional() {
        // 模拟需要插入12名员工到数据库
        List<Employee> list = IntStream.range(0, 12)
                .mapToObj(i -> {
                    Employee employee = new Employee();
                    employee.setEmployeeId(i);
                    employee.setEmployeeName("三丰" + i);
                    employee.setAge(i + 100);
                    return employee;
                })
                .collect(Collectors.toList());
        employeeService.saveThreadByTransactional(list);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/saveThreadRollBack")
    public ResponseEntity saveThreadRollBack() throws SQLException {
        // 模拟需要插入12名员工到数据库
        List<Employee> list = IntStream.range(0, 12)
                .mapToObj(i -> {
                    Employee employee = new Employee();
                    employee.setEmployeeId(i);
                    employee.setEmployeeName("三丰" + i);
                    employee.setAge(i + 100);
                    return employee;
                })
                .collect(Collectors.toList());
        employeeService.saveThreadRollBack(list);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
