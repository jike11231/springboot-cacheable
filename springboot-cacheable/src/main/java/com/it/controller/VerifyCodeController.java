package com.it.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import com.it.resp.VerifyCodeResp;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 验证码工具类
 */
@RestController
public class VerifyCodeController {
    @Resource
    RedisTemplate<String, String> redisTemplate;

    /**
     * 生成验证码
     *
     * 方法一 ShearCaptcha
     * 图片格式
     * session存储
     * 接口需添加白名单放行
     *
     * @param request HttpServletRequest
     */
    @GetMapping("/verify")
    public void verify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        ShearCaptcha shearCaptcha = CaptchaUtil.createShearCaptcha(150, 40, 5, 4);
        //图形验证码写出，可以写出到文件，也可以写出到流
        shearCaptcha.write(response.getOutputStream());
        //获取验证码中的文字内容
        request.getSession().setAttribute("verifyCode", shearCaptcha.getCode());
    }

    /**
     * 生成验证码
     *
     * 方法二 LineCaptcha
     * 图片格式
     * session存储
     * 接口需添加白名单放行
     *
     * @param request HttpServletRequest
     */
    @GetMapping("/verifyTwo")
    public void verifyTwo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(150, 40, 5, 4);
        //图形验证码写出，可以写出到文件，也可以写出到流
        ImageIO.write(lineCaptcha.getImage(), "JPEG", response.getOutputStream());
        //获取验证码中的文字内容
        request.getSession().setAttribute("verifyCode", lineCaptcha.getCode());
    }

    /**
     * 方法三 ShearCaptcha
     * 图片的base64编码字符串
     * session存储
     * 接口需添加白名单放行
     *
     * @param request HttpServletRequest
     * @return String
     */
    @GetMapping("/getVerify")
    public String getVerify(HttpServletRequest request) {
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        ShearCaptcha shearCaptcha = CaptchaUtil.createShearCaptcha(150, 40, 5, 4);
        //获取验证码中的文字内容
        request.getSession().setAttribute("verifyCode", shearCaptcha.getCode());
        String base64String = "";
        try {
            base64String = "data:image/png;base64," + shearCaptcha.getImageBase64();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64String;
    }

    /**
     * 方法四 LineCaptcha
     * 图片的base64编码字符串
     * session存储
     * 接口需添加白名单放行
     *
     * @param request HttpServletRequest
     * @return String
     */
    @GetMapping("/getVerifyTwo")
    public String getVerifyTwo(HttpServletRequest request) {
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(150, 40, 5, 4);
        //获取验证码中的文字内容
        request.getSession().setAttribute("verifyCode", lineCaptcha.getCode());
        String base64String = "";
        try {
            //返回 base64
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(lineCaptcha.getImage(), "JPEG", bos);
            byte[] bytes = bos.toByteArray();
            Base64.Encoder encoder = Base64.getEncoder();
            base64String = "data:image/png;base64," + encoder.encodeToString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64String;
    }

    /**
     * 方法五 ShearCaptcha
     * 图片的base64编码字符串
     * redis存储
     * 接口需添加白名单放行
     *
     * @return String
     */
    @GetMapping("/getVerifyThree")
    public VerifyCodeResp getVerifyThree() {
        String captchaKey = UUID.randomUUID().toString();
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        ShearCaptcha shearCaptcha = CaptchaUtil.createShearCaptcha(150, 40, 5, 0);
        // 存入redis并设置过期时间为30分钟
        redisTemplate.opsForValue().set("captcha:" + captchaKey, shearCaptcha.getCode(), 30L, TimeUnit.MINUTES);
        String base64String = "";
        try {
            base64String = "data:image/png;base64," + shearCaptcha.getImageBase64();
        } catch (Exception e) {
            e.printStackTrace();
        }
        VerifyCodeResp verifyCodeResp = new VerifyCodeResp();
        verifyCodeResp.setCaptchaKey(captchaKey);
        verifyCodeResp.setCaptchaImg(base64String);
        return verifyCodeResp;
    }

    /**
     * 方法六 LineCaptcha
     * 图片的base64编码字符串
     * redis存储
     * 接口需添加白名单放行
     *
     * @return String
     */
    @GetMapping("/getVerifyFour")
    public VerifyCodeResp getVerifyFour() {
        String captchaKey = UUID.randomUUID().toString();
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(150, 40, 5, 4);
        // 存入redis并设置过期时间为30分钟
        redisTemplate.opsForValue().set("captcha:" + captchaKey, lineCaptcha.getCode(), 30L, TimeUnit.MINUTES);
        String base64String = "";
        try {
            //返回 base64
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(lineCaptcha.getImage(), "JPEG", bos);
            byte[] bytes = bos.toByteArray();
            Base64.Encoder encoder = Base64.getEncoder();
            base64String = "data:image/png;base64," + encoder.encodeToString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        VerifyCodeResp verifyCodeResp = new VerifyCodeResp();
        verifyCodeResp.setCaptchaKey(captchaKey);
        verifyCodeResp.setCaptchaImg(base64String);
        return verifyCodeResp;
    }
}