package com.it.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.it.entity.Mountain;
import com.it.mapper.MountainMapper;
import com.it.service.UserMountainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * 实现类
 *
 * @author hua
 */
@Service
public class UserMountainServiceImpl extends ServiceImpl<MountainMapper, Mountain> implements UserMountainService {

    @Autowired
    private MountainMapper mountainMapper;

    /**
     * 获取所有上古神山相关人员信息
     * @return List
     */
    @Override
    public List<Mountain> queryAllUser() {
        return mountainMapper.queryAllUser();
    }
}
