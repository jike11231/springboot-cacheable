package com.it.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.it.constant.CacheTimes;
import com.it.entity.User;
import com.it.service.AdminService;
import com.it.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 用户实现类
 *
 * @author hua
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    UserService userService;

    /**
     * 保存String类型的数据,
     */
    @Override
    @CachePut(value = "张三")
    public void insert1() {
    }

    /**
     * 保存对象 形参方式保存
     */
    @Override
    @CachePut(value = "user", key = "#user.id", unless = "#user eq null")
    public void insert2(User user) {
    }
    // key = "#user.id"： 取形参user中的id值, 作为key名称
    // value = "user": 形参user, 作为value值
    // unless = "#user eq null ：判断 形参user对象是空的,若unless的结果为true，则(方法执行后的功能)不生效；若unless的结果为false，则(方法执行后的)功能生效。
    // 生效则会将形参user对象以json格式字符串保存到redis

    /**
     * 保存对象 返回值方式保存
     */
    @Override
    @CachePut(value = "user", key = "#result.id", unless = "#result eq null")
    public User insert3() {
        LambdaQueryWrapper<User> wrapper = Wrappers.<User>lambdaQuery();
        wrapper.eq(User::getName,"小花花");
        User user = userService.getOne(wrapper);
        return user;
    }
    // key = "#result.id"： 取返回值user中的id值, 作为key名称
    // value = "user": 取返回值user, 作为value值
    // unless = "#result eq null： 判断 方法返回值是空的, 若unless的结果为true，则(方法执行后的功能)不生效；若unless的结果为false，则(方法执行后的)功能生效。
    // 生效则会将返回值user对象以json格式字符串保存到redis

    @Override
    @Cacheable(value = CacheTimes.D1, key = "#root.methodName", unless = "#result == null || #result.size() < 1", condition = "#skip != null")
    public List<String> getList(String skip) {
        return Arrays.stream(UUID.randomUUID().toString().split("-")).collect(Collectors.toList());
    }

    /*
        说明：@Cacheable注解有三个参数，value是必须的，还有key和condition。
        第一个参数value：指明了缓存将被存到什么地方.上面的代码保证findPersonByName的返回值Person对象将被存储在"employee"中。
        第二个参数key：指定key,任何存储在缓存中的数据为了高速访问都需要一个key。Spring默认使用被@Cacheable注解的方法的签名来作为key, 当然你可以重写key, 自定义key可以使用SpEL表达式。
        在findEmployee()的注解中"#name"是一个SpEL表达式，他将使用findPersonByName()方法中的name参数作为key。
        第三个参数condition(可选)：
        功能1：缓存的条件, 决定是否缓存。@Cacheable的最后一个参数是condition（可选），同样的，也是引用一个SpEL表达式。但是这个参数将指明方法的返回结果是否被缓存。
        功能2：condition则判定了是否走换成逻辑。如果age<25，即condition是false，就不去读取缓存，而是直接执行当前目标方法，返回结果。
        上面的例子中，只有年龄小于25的时候才被缓存
     */
    @Cacheable(value = "employee", key = "#name" ,condition = "#age < 25")
    public User findEmployee(String name, int age) {
        return new User();
    }
}
