package com.it.service.impl;

import com.it.service.PayService;
import org.springframework.stereotype.Component;

/**
 * @description: 微信支付实现层
 */
@Component("WxPay")
public class WxPayServiceImpl implements PayService {
    @Override
    public boolean pay(String orderId) {
        System.out.println("微信支付-----------------------");
        return true;
    }
}