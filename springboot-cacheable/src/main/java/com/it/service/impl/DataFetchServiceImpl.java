package com.it.service.impl;

import com.it.entity.User;
import com.it.mapper.UserMapper;
import com.it.service.DataFetchService;
import com.it.utils.RateLimitedDataFetcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class DataFetchServiceImpl implements DataFetchService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RateLimitedDataFetcher rateLimitedDataFetcher;

    @Override
    public List<User> dataFetchTask() {
        List<User> userList = null;
        try {
            userList = rateLimitedDataFetcher.fetchData(2).get();
        } catch (InterruptedException | ExecutionException e) {
           log.error("DataFetchServiceImpl dataFetchTask error:{}",e.getMessage());
        }
        return userList;
    }
}
