package com.it.service;

import com.it.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * UserService
 *
 * @author hua
 */
public interface UserService extends IService<User>{

    /**
     * save use, put redis cache
     * @param user user data
     * @return saved user data
     */
     User insert(User user);

    /**
     * find user by id,redis cacheable
     * @param userId user id
     * @return if exist return the user, else return null
     */
     User findUser(Integer userId);

    /**
     * find user by id,self key,redis cacheable
     * @param userId user id
     * @return if exist return the user, else return null
     */
    User findUserSelfKey(Integer userId);

    /**
     * delete user by id, and remove redis cache
     * @param userId user id
     */
     void delete(Integer userId);
}
