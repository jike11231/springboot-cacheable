package com.it.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.it.entity.Employee;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeService extends IService<Employee> {
    /**
     * 使用@Transactional测试多线程回滚失败
     */
    void saveThreadByTransactional(List<Employee> employeeList);

    /**
     * 使用手动操作事务测试多线程回滚成功
     */
    void saveThreadRollBack(List<Employee> employeeList) throws SQLException;
}
