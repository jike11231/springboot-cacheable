package com.it.service;

/**
 * @description: 支付服务接口
 */
public interface PayService {
    //支付接口
    boolean pay(String orderId);
}
