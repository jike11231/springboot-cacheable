package com.it.demo;

import com.it.constant.CacheTimes;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 实例
 *
 * @author hua
 */
public class Demo {

    /**
     * 读取缓存
     *
     * @param skip
     * @return
     */
    @Cacheable(value = CacheTimes.D1, key = "#root.methodName", unless = "#result == null || #result.size() < 1", condition = "#skip != null")
    public List<String> getList(String skip) {
        return Arrays.stream(UUID.randomUUID().toString().split("-")).collect(Collectors.toList());
    }


    /**
     * 放入缓存
     *
     * @param list
     * @param condition
     * @return
     */
    @CachePut(value = CacheTimes.D1, key = "#root.methodName", unless = "#result == null || #result.size() < 1", condition = "#condition")
    public List<String> setListCache(List<String> list, boolean condition) {
        if (list != null && list.size() > 0) {
            return list;
        }
        return Arrays.stream(UUID.randomUUID().toString().split("-")).collect(Collectors.toList());
    }

    /**
     * 放入缓存
     *
     * @param key
     * @param value
     * @return
     */
    @CachePut(value = CacheTimes.D1, key = "#key")
    public Object setKV(String key, Object value) {
        return value;
    }

    /**
     * 删除全部缓存数据
     *
     * @param condition
     */
    @CacheEvict(value = CacheTimes.D1, condition = "#condition", allEntries = true)
    public void clearCache(boolean condition) {
        //……
    }

    /**
     * 指定key删除
     *
     * @param key
     */
    @CacheEvict(value = CacheTimes.D1, allEntries = false, key = "#key")
    public void clearCache(String key) {

    }


}
