package com.it.resp;

import lombok.Data;
import java.io.Serializable;

/**
 * 验证码响应体
 */
@Data
public class VerifyCodeResp implements Serializable {
    /**
     * header头参数：Captcha-Key
     */
    private String captchaKey;

    /**
     * 验证码图片
     */
    private String captchaImg;
}