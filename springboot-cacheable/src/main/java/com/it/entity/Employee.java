package com.it.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 员工
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "employee")
public class Employee {
    @TableField(value = "employee_id")
    private Integer employeeId;

    @TableField(value = "employee_name")
    private String employeeName;

    @TableField(value = "age")
    private Integer age;
}
