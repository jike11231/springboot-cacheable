package com.it;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.retry.annotation.EnableRetry;

/**
 * 主启动类
 * EnableCaching允许使用注解进行缓存
 * 添加注解@EnableRetry
 * @author hua
 */
@EnableRetry
@EnableCaching
@SpringBootApplication
@MapperScan(basePackages = "com.it.mapper")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
