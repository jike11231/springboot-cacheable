package com.it;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            String[] split = sc.nextLine().split(" ");
            int arrayLength = split.length;
            int[] arr = new int[arrayLength];
            for (int i = 0; i <arrayLength ; i++) {
                arr[i] = Integer.parseInt(split[i]);
            }
            int min = Integer.MAX_VALUE;
            int[] minArr = new int[2];
            for (int i = 0; i <arrayLength-1 ; i++) {
                for (int k = i+1; k < arrayLength; k++) {
                    int sum = Math.abs(arr[i]+arr[k]);
                    if(sum < min){
                        min = sum;
                        minArr[0] = arr[i];
                        minArr[1] = arr[k];
                    }
                }
            }
            Arrays.sort(minArr);
            for (int i:minArr) {
                System.out.print(i+" ");
            }
            System.out.println(min);
        }
        sc.close();
    }
}
