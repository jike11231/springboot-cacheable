package com.it;

import com.it.service.impl.SpringRetryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class SpringRetryServiceTest {
    @Autowired
    private SpringRetryService springRetryService;

    @Test
    void contextLoads() {
        boolean result = springRetryService.call(100);
        log.info("方法返回结果为: {}", result);
    }
}
