package com.it;

import com.it.service.AdminService;
import com.it.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationTest {
    @Autowired
    private AdminService adminService;

    @Autowired
    private UserService userServic;

    @Test
    void contextLoads() {
    }

    /*
        最后总结一下流程，当执行到一个被@Cacheable注解的方法时，
        Spring首先检查condition条件是否满足，如果不满足，执行方法，返回；
        如果满足，在value所命名的缓存空间中查找使用key存储的对象，
        如果找到，将找到的结果返回，
        如果没有找到执行方法，将方法的返回值以key-对象的方式存入value缓存中，然后方法返回。
     */


}
