package com.it;

public class Test3 {
    public static void main(String[] args) {
        //双重for循环，里层满足条件，跳出里层和外层循环
        f1:for (int i = 0; i <= 10; i++) {
            f2:for (int j = 0; j <= 10; j++) {
            /*
                if(j == 5){
                    System.out.println(i+"*********"+j);
                    break f2;
                }
                */
                    if(j == 5){
                        System.out.println(i);
                        System.out.println(j);
                        break f1;
                    }

            }
        }
    }
}
