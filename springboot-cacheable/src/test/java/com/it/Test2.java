package com.it;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        Action[] actions = new Action[N];
        for (int i = 0; i <N ; i++) {
            int startNum = sc.nextInt();
            int durationNum = sc.nextInt();
            actions[i] = new Action(startNum,durationNum);
        }

        Arrays.sort(actions,Comparator.comparingInt(p->(p.durationNum+p.startNum)));
        int count = 0;
        int lastNum = 0;
        for (int i = 0; i <N ; i++) {
            if(actions[i].startNum >= lastNum){
                count++;
                lastNum = actions[i].startNum+ actions[i].durationNum +15;
            }
        }
        System.out.println(count);
    }

    public static class Action {
        int startNum;
        int durationNum;
        public Action(int x, int y){
            startNum = x;
            durationNum = y;
        }
    }
}
