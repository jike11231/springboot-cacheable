package com.it;

import java.util.Arrays;
import java.util.Scanner;

public class Test1 {
    public  static int splitArray(int m,int[] nums)
    {
        int maxNum = 0;
        int sumNum = 0;
        for (int num : nums) {
            maxNum = Math.max(maxNum, num);
            sumNum += num;
        }

        int left = maxNum;
        int right = sumNum;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (splitNums(mid, nums) > m) {
                left = mid + 1;
            } else {
                left = mid;
            }
        }
        return left;
    }

    public static int splitNums(int maxNum, int[] nums) {
        int currentSum = 0;
        int count = 1;
        for (int num : nums) {
            if (currentSum + num > maxNum) {
                count++;
                currentSum = 0;
            }
            currentSum += num;
        }
        return count;
    }
/*
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int[] nums;
        while(sc.hasNext()){
            //输入

        }
        nums


    }*/
}
